TicTacToe kata

The project can be run by cloning it to your local machine and opening it into Android studio after which you can click run.

I made this project using MVVM since I had not worked with it before and I saw this as a perfect oppurtunity to try it out.

- I started out by making the layouts which I absolutely love making
- Then I started reading up on MVVM and livedata
- I added the right packages for kotlin 
- I tested with MVVM and livedata a little bit
- First thing I did for the tic tac toe game was made it so I could add X or O to the game board
- Than I wrote a test to check how that went
- Than I added the ability to swap between X and O and wrote a test for that
- Lastly I added the game logic to check if someone won the game or it was a draw and wrote a test for that.