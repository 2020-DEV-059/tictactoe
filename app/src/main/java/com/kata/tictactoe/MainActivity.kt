package com.kata.tictactoe

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.kata.tictactoe.model.GameState
import com.kata.tictactoe.model.Move
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: GameAdapter
    private val viewModel: GameViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setActivityFullScreen()

        gameRefresh.setOnClickListener { viewModel.startNewGame() }

        adapter = GameAdapter { viewModel.onGameTileClick(it) }
        gameGrid.adapter = adapter

        val gameObserver = Observer<GameState> { gameState ->
            adapter.updateItems(gameState.game)
            turnState.setImageResource(if (gameState.turnPlayer1) R.drawable.ic_x else R.drawable.ic_o)
            if (gameState.gameFinished) showWinnerDialog(gameState.winner)
        }
        viewModel.getGameState().observe(this, gameObserver)
        viewModel.startNewGame()

    }

    private fun setActivityFullScreen() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        supportActionBar?.hide()
        window.statusBarColor = Color.TRANSPARENT
    }

    private fun showWinnerDialog(winner: String?) {
        AlertDialog.Builder(this).apply {
            setTitle("winner winner chicken dinner!")
            setMessage(winner)
            setCancelable(false)
            setPositiveButton("Start again") { dialog, _ ->
                viewModel.startNewGame()
                dialog.dismiss()
            }
        }.create().show()
    }
}
