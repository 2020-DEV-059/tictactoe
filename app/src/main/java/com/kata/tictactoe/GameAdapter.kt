package com.kata.tictactoe

import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.kata.tictactoe.Extensions.inflate
import com.kata.tictactoe.model.Move
import kotlinx.android.synthetic.main.ttt_item.view.*

class GameAdapter(private val listener: (Int) -> Unit) :
    RecyclerView.Adapter<GameAdapter.ViewHolder>() {

    private var items: List<Move> = listOf()

    fun updateItems(items: List<Move>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(parent.inflate(R.layout.ttt_item))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(position, items[position], listener)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int, item: Move, listener: (Int) -> Unit) = with(itemView) {
            when (item) {
                Move.NONE -> moveItem.setImageResource(android.R.color.transparent)
                Move.O -> moveItem.setImageResource(R.drawable.ic_o)
                Move.X -> moveItem.setImageResource(R.drawable.ic_x)
            }
            moveItem.setOnClickListener { listener(position) }
        }
    }

}