package com.kata.tictactoe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kata.tictactoe.model.Move

object Extensions {
    fun ViewGroup.inflate(layoutRes: Int): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, false)
    }

    fun List<List<Move>>.getColumn(columnId: Int): List<Move> {
        return listOf(this[0][columnId], this[1][columnId], this[2][columnId])
    }

    fun Move.winnerText(): String {
        return when (this) {
            Move.NONE -> "It's a draw!"
            Move.X -> "X wins!"
            Move.O -> "O wins!"
        }
    }
}