package com.kata.tictactoe

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kata.tictactoe.Extensions.getColumn
import com.kata.tictactoe.Extensions.winnerText
import com.kata.tictactoe.model.GameState
import com.kata.tictactoe.model.Move


class GameViewModel : ViewModel() {

    private val gameState: MutableLiveData<GameState> = MutableLiveData()
    private val newGameState: GameState = GameState(true, MutableList(9) { Move.NONE })

    fun getGameState(): LiveData<GameState> = gameState

    fun startNewGame() {
        gameState.value = newGameState
    }

    fun onGameTileClick(position: Int) {
        gameState.value?.let {
            val game: MutableList<Move> = it.game.toMutableList()
            val player1turn: Boolean = it.turnPlayer1

            if (game[position] == Move.NONE) {
                if (player1turn) {
                    game[position] = Move.X
                } else {
                    game[position] = Move.O
                }
                this.gameState.value = checkGameState(GameState(!player1turn, game))
            }
        }
    }

    private fun checkGameState(gameState: GameState): GameState {
        val gameMatrix: List<List<Move>> = arrayListOf(
            gameState.game.subList(0, 3),
            gameState.game.subList(3, 6),
            gameState.game.subList(6, 9)
        )

        //check horizontal
        for (h in 0..2) {
            if (!gameMatrix[h].contains(Move.NONE)) {
                val move = gameMatrix[h][0]
                if (move == gameMatrix[h][1]
                    && move == gameMatrix[h][2]
                ) return gameState.copy(
                    gameFinished = true,
                    winner = move.winnerText()
                )
            }
        }

        //check vertical
        for (v in 0..2) {
            if (!gameMatrix.getColumn(v).contains(Move.NONE)) {
                val move = gameMatrix[0][v]
                if (move == gameMatrix[1][v]
                    && move == gameMatrix[2][v]
                ) return gameState.copy(
                    gameFinished = true,
                    winner = move.winnerText()
                )
            }
        }

        //check Diagonals
        val leftDiagonal = gameMatrix[0][0]
        if (leftDiagonal != Move.NONE) {
            if (leftDiagonal == gameMatrix[1][1]
                && leftDiagonal == gameMatrix[2][2]
            ) return gameState.copy(
                gameFinished = true,
                winner = leftDiagonal.winnerText()
            )
        }

        val rightDiagonal = gameMatrix[0][2]
        if (rightDiagonal != Move.NONE) {
            if (rightDiagonal == gameMatrix[1][1]
                && rightDiagonal == gameMatrix[2][0]
            ) return gameState.copy(
                gameFinished = true,
                winner = rightDiagonal.winnerText()
            )
        }


        if (!gameState.game.contains(Move.NONE)) return gameState.copy(
            gameFinished = true,
            winner = Move.NONE.winnerText()
        )
        return gameState
    }

}

