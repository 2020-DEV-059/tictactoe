package com.kata.tictactoe.model

enum class Move {
    O, X, NONE
}