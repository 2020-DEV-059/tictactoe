package com.kata.tictactoe.model

data class GameState(
    val turnPlayer1: Boolean,
    val game: List<Move>,
    val gameFinished: Boolean = false,
    val winner: String? = null
)