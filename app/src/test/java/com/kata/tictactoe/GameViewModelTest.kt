package com.kata.tictactoe

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kata.tictactoe.TestUtils.observeOnce
import com.kata.tictactoe.model.Move
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

class GameViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel: GameViewModel

    @Before
    fun setup() {
        viewModel = GameViewModel()
    }

    @Test
    fun startNewGame_success() {
        viewModel.startNewGame()
        viewModel.getGameState().observeOnce {
            assertTrue(it.turnPlayer1)
            assert(it.game.size == 9)
        }
    }

    @Test
    fun onTileGameClick_empty_tile() {
        viewModel.startNewGame()
        viewModel.onGameTileClick(0)
        viewModel.getGameState().observeOnce {
            assertFalse(it.turnPlayer1)
            assert(it.game[0] == Move.X)
        }
    }

    @Test
    fun onTileGameClick_horizontal_winner() {
        viewModel.startNewGame()
        viewModel.onGameTileClick(0)
        viewModel.onGameTileClick(3)
        viewModel.onGameTileClick(1)
        viewModel.onGameTileClick(4)
        viewModel.onGameTileClick(2)
        viewModel.getGameState().observeOnce {
            assertTrue(it.gameFinished)
            assertEquals("X wins!", it.winner)
        }

    }

    @Test
    fun onTileGameClick_vertical_winner() {
        viewModel.startNewGame()
        viewModel.onGameTileClick(1)
        viewModel.onGameTileClick(0)
        viewModel.onGameTileClick(4)
        viewModel.onGameTileClick(3)
        viewModel.onGameTileClick(8)
        viewModel.onGameTileClick(6)
        viewModel.getGameState().observeOnce {
            assertTrue(it.gameFinished)
            assertEquals("O wins!", it.winner)
        }
    }

    @Test
    fun onTileGameClick_diagonal_winner() {
        viewModel.startNewGame()
        viewModel.onGameTileClick(0)
        viewModel.onGameTileClick(1)
        viewModel.onGameTileClick(4)
        viewModel.onGameTileClick(3)
        viewModel.onGameTileClick(8)
        viewModel.getGameState().observeOnce {
            assertTrue(it.gameFinished)
            assertEquals("X wins!", it.winner)
        }
    }

    @Test
    fun onTileGameClick_draw() {
        viewModel.startNewGame()
        viewModel.onGameTileClick(0)
        viewModel.onGameTileClick(1)
        viewModel.onGameTileClick(2)
        viewModel.onGameTileClick(4)
        viewModel.onGameTileClick(3)
        viewModel.onGameTileClick(5)
        viewModel.onGameTileClick(7)
        viewModel.onGameTileClick(6)
        viewModel.onGameTileClick(8)
        viewModel.getGameState().observeOnce {
            assertTrue(it.gameFinished)
            assertEquals("It's a draw!", it.winner)
        }
    }


}
